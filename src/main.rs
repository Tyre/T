use t_backend_llvm::ScopeBackend;
use t_ree::scope::Scope;

fn main() {
    let mut args = std::env::args();
    args.next();
    let mut context = Scope::new();
    let mut out = None;
    while let Some(arg) = args.next() {
        match &arg[..] {
            "-o" => out = args.next(),
            _ => context.parse_from_file(&arg[..], t_parser_old::parse),
        }
    }

    context.build(out);
}
