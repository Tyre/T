This is a WIP programming language, which aims to be similar to C, especially according to portability and features, but more consistent.

It might have a different syntax, and maybe there will be multiple different syntaxes (one similar to C, one indentation based like python and one pure s-expression one, similar to lisp, or even graphical representations)

But for now, it's just something that kind of works sometimes, so come back later.

Feel free to contact me if you are interest or want to support me.

# What it will not do

The included features are mostly limited to features provided directly by LLVM, except they exist in C, but not LLVM.
Features much more complicated than C will not be included in this language.

These features are not intended to be included in T:
* function overloading
* namespaces
* definition of new types (structs are just tuples, but you can give them names)
* generics/templates (maybe some simple form of macros which can be used for something like that)
* 

# Future projects

This language will be used for a more advanced language, which will interact with T in a similar way as C++ interacts with C.

The main focus of that language is an advanced type system, but first this language (T) needs to work well.

# Parsing

Steps:
1. any simple representation (text, graphical)
2. s-expressions
2. macro expansion (preprocessor)
3. AST
4. LLVM
5. direct execution or conversion to object files or executables

# Quickstart

Open cloned directory in terminal. Run using `cargo run -- <args...>` or install using `cargo install --path .` and run using `t <args...>`.

The arguments (`args...`) work similar to the arguments of common C compilers.
You just specify one or multiple files, which will read in, optionally you specify an output file using the `-o` flag.

If you call it without an output file, the program is executed immediately, instead of creating a file having a default name.

